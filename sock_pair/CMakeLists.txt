cmake_minimum_required(VERSION 3.14.0)
project(tchain-lite VERSION 0.1.0 LANGUAGES C)
set(CMAKE_C_STANDARD 99)
set(CMAKE_C_STANDARD_REQIRED TRUE)
set(CMAKE_C_FLAGS "-Wall -Wno-missing-braces -O2")

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

add_executable(sock_pair sock_pair.c)
