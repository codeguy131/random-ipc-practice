#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <unistd.h>

int main(int argc, char **argv) {
  fprintf(stdout, "Hello, this is a test\n");
  int ipc[2];
  if (socketpair(AF_UNIX, SOCK_STREAM, 0, ipc) < 0) {
    perror("socketpair");
    _exit(-1);
  }

  char buff[1024];

  pid_t pid = fork();
  if (pid < 0) {
    perror("fork");
    _exit(-1);
  } else if (pid == 0) {
    close(ipc[0]);

    strcpy(buff, "This is the return msg\0");

    if (write(ipc[1], buff, sizeof(buff)) < 0) {
      perror("child write");
      _exit(-1);
    }

    fprintf(stdout, "CHILD SENT: %s\n", buff);

    if (read(ipc[1], &buff, sizeof(buff)) < 0) {
      perror("child read");
      _exit(-1);
    }

    fprintf(stdout, "CHILD READ: %s\n", buff);

    memset(&buff, 0, strlen(buff));

  } else {
    close(ipc[1]);
    if (read(ipc[0], buff, sizeof(buff)) < 0) {
      perror("parent read");
      _exit(-1);
    }

    fprintf(stdout, "PARENT READ: %s\n", buff);

    strcpy(buff, "Hello from parent\0");
    if (write(ipc[0], buff, sizeof(buff)) < 0) {
      perror("write");
      _exit(-1);
    }

    fprintf(stdout, "PARENT SENT: %s\n", buff);

    memset(&buff, 0, strlen(buff));

    wait(NULL);
  }

  return 0;
}
