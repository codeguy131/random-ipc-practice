#include <ctype.h>
#include <fcntl.h>
#include <semaphore.h>
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>

#define SHARED_MEM_SEG "scratch_shared_memory_seg"
#define SHARED_FILE_MODE O_CREAT | O_RDWR

static sem_t sem1, sem2;

int write_to_shm(void) {

  // Open file for shared memory
  int shfd = shm_open(SHARED_MEM_SEG, SHARED_FILE_MODE, S_IRUSR | S_IWUSR);
  if (shfd < 0) {
    perror("shm_open");
    _exit(-1);
  }

  // Truncate new file
  if (ftruncate(shfd, sizeof(shfd)) < 0) {
    perror("ftruncate");
    _exit(-1);
  }

  // Map shared seg into user space
  int *buff =
      mmap(NULL, getpagesize(), PROT_READ | PROT_WRITE, MAP_SHARED, shfd, 0);
  if (buff == NULL) {
    perror("mmap");
    _exit(-1);
  }

  // Initialize sephamore
  if (sem_init(&sem1, 1, 0) < 0) {
    perror("sem_init");
    _exit(-1);
  }
  if (sem_init(&sem2, 1, 0) < 0) {
    perror("sem_init");
    _exit(-1);
  }

  // Wait for sem1 to be posted
  if (sem_wait(&sem1) < 0) {
    perror("sem_wait");
    _exit(-1);
  }

  // Do stuff
  char buff_string[1024] = "\n";
  strcpy(buff_string, "WRITE TEST");
  if (write(*buff, buff_string, sizeof(buff_string)) < 0) {
    perror("writer read");
    _exit(-1);
  }

  memset(buff_string, 0, sizeof(buff_string));

  fprintf(stdout, "SENT TO READER: %s\n", buff_string);

  // Unlock sem2
  if (sem_post(&sem2) < 0) {
    perror("sem_post");
    _exit(-1);
  }

  // Close shared memory file
  close(shfd);

  // Unlink shared memory
  shm_unlink(SHARED_MEM_SEG);
  munmap(buff, sizeof(*buff));

  return 0;
}

/*******************
 * Reader function
 ******************/
int read_from_shm(void) {

  // Open shared memory file
  int shfd = shm_open(SHARED_MEM_SEG, O_RDWR, S_IRUSR | S_IWUSR);
  if (shfd < 0) {
    perror("shm_open");
    _exit(-1);
  }

  // Map into caller space
  int *buff =
      mmap(NULL, sizeof(*buff), PROT_READ | PROT_WRITE, MAP_SHARED, shfd, 0);
  if (buff == MAP_FAILED) {
    perror("mmap");
    _exit(-1);
  }

  char string_buff[1024];
  if (read(*buff, string_buff, sizeof(string_buff)) < 0) {
    perror("reader read");
    _exit(-1);
  }

  fprintf(stdout, "READ FROM WRITER: %s\n", string_buff);
  memset(string_buff, 0, sizeof(string_buff));

  // Unlock shared memory
  if (sem_post(&sem1) < 0) {
    perror("sem_post");
    _exit(-1);
  }

  // Wait for writer to finish
  if (sem_wait(&sem2) < 0) {
    perror("sem_wait");
    _exit(-1);
  }

  // Write modified data
  write(*buff, string_buff, sizeof(string_buff));

  fprintf(stdout, "READER SENT: %s\n", string_buff);

  // Close shared memory file
  close(shfd);

  // Unlink shared memory segment
  munmap(buff, sizeof(buff));
  shm_unlink(SHARED_MEM_SEG);

  return 0;
}

int main() {
  pid_t pid = fork();
  if (pid < 0) {
    perror("fork");
    _exit(-1);
  } else if (pid == 0) {
    if (read_from_shm() != 0) {
      fprintf(stderr, "Problem reading from shm\n");
      _exit(-1);
    }
  } else {
    if (write_to_shm() != 0) {
      fprintf(stderr, "Problem writing to shared memory\n");
      _exit(-1);
    }
    wait(NULL);
  }
}
