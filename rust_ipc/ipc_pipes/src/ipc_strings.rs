use std::sync::mpsc::channel;
use std::thread::spawn;

fn main() {
    let (tx, rx) = channel();
    let sender = spawn(move || {
        tx.send("Hello from spawned process").unwrap();
    });

    let rec = spawn(move || {
        let val = rx.recv().unwrap();
        println!("{:?}", val);
    });

    sender.join().unwrap();
    rec.join().unwrap();
}
