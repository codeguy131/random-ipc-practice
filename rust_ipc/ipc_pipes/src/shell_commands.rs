use std::io::prelude::*;
use std::process::{Command, Stdio};

fn main() {
    let process = match Command::new("ls")
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn()
    {
        Err(why) => panic!("Spawn failed {}", why),
        Ok(process) => process,
    };

    let mut s = String::new();
    match process.stdout.unwrap().read_to_string(&mut s) {
        Err(why) => panic!("Couldnt read from stdout {}", why),
        Ok(_) => print!("Output of command:\n{}", s),
    }
}
